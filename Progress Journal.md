# Progress Journal
#### Sand and Smoke; Crying and Blood - Week #1
---
There's a new application and a new way of doing things. *Docker* is our new application, which currently, i don't know what it does. *GitHub* is the hosting website for our course at the moment. This I have a **little** more information on as Jeff ran me through how it worked.  
I've chosen to learn the **ASP.net** GUI option, namely because I have an interest in working with an alternative to *PHP*, and my understanding is the *ASP.net* will enable me to replace the *PHP* that I know.  
We're also learning **Markdown** language for writing information. It appears to be *simple* enough, as it's mostly a *written* language with few symbols.  
So to recap, what are we studing?  
* Asp.net
* Github hosted learning module
* Markdown language
---
#### A fear of spiders and old age - Week #2
---
> I thought that I had answered this previously, so I guess I forgot to gitbash it up my bitbucket - James Beck  
Also I found out how to use block quoes with markdown language which is pretty sweet.

From this week I remember not having much to contribute, even the blockquotes which at the time I didn't know how to do. My progress with docker was poor. I did get an understanding of how it was working, I just couldn't see the output of the websites that I was creating.  
To mitigate this what I've done is installed the programs that I need, excluding docker. It's unfortunate because I won't get to practice with using containers, however, I can't progress with ASPnet without being able to see what I'm doing, so this is better  

---
#### Cripple hearts, crippled minds - Week #3
---
> "Do I know how to use block quotes?"
* yes
* no

Turns out the answer is yes, I still know how to use blockquotes. Markdown language is quite easy to write it, it's pretty good. I also believe that Jeffs to-bcs.nz was written in markdown, and if so I can see why he did it.

Good news, I've finally made progress with my ASPnet, I completed the week 2 project, though it's super messy - *it does work*.

To further my understanding of ASPnet, which I've found confusing, I've been utilising Lynda.com. This has been pretty good at show how the MVC model is used. I really think that I could start doing some website design some now.

**On the subject of the Lynda.com course, I didn't realise that you could utilise the URL bar to input parameters into a method, which is super cool**

---
#### Dystopia 7: A world of coding - Week #4
---
I think it was a mistake to utilise the *Lynda.com Course*. Don't get me wrong, it certainly was helpful, but it's too fast and too much of a **this is how you get it done** when really what I need it to be shown a little piece and then **get it done myself**.  
What I've found very helpful is the microsoft documentation that's available. What I've been doing is going through that and playing around with each component, just a little bit.  
My belief is that *I now* have a better understanding of how the basics work. To the point of this entry I've only really practiced two things, controllers and views. Nothing serious at all. The kicker *I get them now*. I even made a two sentence goosebumps controller thing, that I thought was pretty cool. Progress is slow, but it's progress. Being shown how to do something is fast, but it's not progress.  
>"I'm on track and it's going to stay that way" - Someday froma wise James
---
#### Stumbling in the dark - Week #5
---
>"Don't forget to continue working on the things you should continue working on" - James Beck  

So I've pretty much screwed up. I'm a 12 days behind on a personal growth schedule for ASPnet due to my own laziness **ffs**. I need to get back on track, and I know that I won't get back on track if I'm not making myself aware of my development, all the time.  
Why I'm saying this is because I ran into another wall with ASPnet *though to be honest, I did learn about partial views **for the win***.
>"One ASPnet website every day bae" - James Beck

---
#### On my way: Week #6
---
>It's been great, I'm getting a lot better at what I do - James Beck  

I've been able to *mostly* be able to do an ASPnet website each day. This has led to, *in my opinion* develop my skills in ASPnet a lot. What have I learned? Try **controllers**, **partial views**, **multiple views**, **Viewdata**, and **ViewBags**.

I think my development is going good and this strategy of ***one ASPnet website per day*** is working out very well.

* I like what I'm studying
* I'm getting better at what I'm studying
* Also, I'm getting better at markdown, *I almost never had to look up the language*.
---
#### It's a brave new world: Week #8
---
This has been a pretty tough week to be completely frank. There are ***so*** many things that I've had to learn, *least of which has been programming*. Man **Json** and **Ajax** was a hard thing for me to figure out. I think that I *literally* spent three hours of wasted times trying to capture the **JSON** array as a string and convert it to an array.
* I did figure some of the stuff out
* I will get it all done, I just have to commit more
* I didn't realise how relaxing it is to make an ASP.Net website for fun, so good :)
---
#### 